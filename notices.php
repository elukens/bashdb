<?php
	include 'config.php';

	$link = mysql_connect($db_host, $db_user, $db_pass);
	if (!$link) {
	    die('Could not connect: ' . mysql_error());
	}
	
	$db_selected = mysql_select_db($db_name, $link);
	if (!$db_selected) {
		die ('Can\'t use: ' . mysql_error());
	}

	$result = mysql_query("select * from notices order by created desc");
	
	echo "<div class=\"scroll\">";
	while ($row = mysql_fetch_row($result)) {
		echo "<p>";
		echo "<b>" . date('m-d-Y', intval($row[1])) . "</b><br>";
		echo nl2br($row[2]);
		echo "</p>";
	}
	echo "</div>";
	
?>