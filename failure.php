<?php
	include 'config.php';
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>bqDB - Brony Quote Database</title>
<link href="main.css" rel="stylesheet" type="text/css" />
<link rel="alternate" type="application/rss+xml" title="BronyDB" href="http://bronydb/rss.xml" />
<script type="text/javascript">
  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>
</head>

<body>
<div id="container">

	<?php include 'header.php'; ?>

	<div id="failure">
		<p>The captcha code input was incorrect. :(</p>	
	</div>

	<?php include 'inc/footer.php'; ?>
	
</div>

</body>
</html>
<?php include 'inc/addCounter.php'; ?>