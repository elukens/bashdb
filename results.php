<?php
	include 'config.php';
	
	$search = $_POST['search'];
	
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>bqDB - Brony Quote Database</title>
<link href="main.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.0/jquery.min.js"></script>
<script type="text/javascript">
$(function() {
	$(".vote").click(function() 
	{

		var id = $(this).attr("id");
		var name = $(this).attr("name");
		var dataString = 'qid=' + id;
		var parent = $(this);
		
		$("span#vote_counts"+id).fadeOut("fast");
		
		if (name=='up')
		{
			$.ajax({
				type: "POST",
				url: "inc/vote-up.php",
				data: dataString,
				cache: false,
				
				success: function(html)
				{
					$("span#vote_counts"+id).html(html);  
				    //fadein the vote count  
					$("span#vote_counts"+id).fadeIn();
					//parent.html(html);
				} 
			});
		}
		else
		{
			$.ajax({
				type: "POST",
				url: "inc/vote-down.php",
				data: dataString,
				cache: false,
				
				success: function(html)
				{
					$("span#vote_counts"+id).html(html);  
				    //fadein the vote count  
					$("span#vote_counts"+id).fadeIn();
					//parent.html(html);
				}
			});
		}
		return false;
	});
});
</script>
</head>

<body>
<div id="container">

	<?php include 'header.php'; ?>
    
    <div id="leftcontainerlarge">
	    <?php include 'inc/results.php'; ?>
    </div>
    <div id="horizdivider"></div>
    
    <?php include 'inc/footer.php'; ?>

</div>

</body>
</html>
<?php include 'inc/addCounter.php'; ?>